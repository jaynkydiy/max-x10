require 'json'
require 'discordrb'
require 'time'
require 'fileutils'
#		This is Burt, a basic message counter.
#
#		Burt's job is to maintain a count of a user's messages 
#		across all channels in the server in a persistent fashion.
#
#		Right now, Burt will store the count in a hash, organized
#		by username+discriminator, for example:
#			messages[beechundmoan5268] = 910209
#		
#		On a regular basis, Burt should dump the hash into a JSON
#		array, which is then saved to file (or committed to a database, or whatever)
#
#		Burt should also emit a warning and enable a backup 
#		"Spew debug data into specified channel" trigger, in case he fails to secure
#		write permissions.



##
##	Import the bot token from the specified filename
##
tokenfile = File.new("./bot_token","r")
token_data = IO.readlines(tokenfile)[0].chomp
tokenfile.close

##
##	Initialize the bot with the specified token
##

bot = Discordrb::Bot.new token: token_data

##
##	Define some global variables
##

#	Where all active data is stored: {'username####': 73}
$message_counter = {}	

#	Users within these roles have permissions to run commands like purge and export
$roles_allowed_to_make_changes = ["Moderator","Jaynky Staff"]

#	The message Burt sends to new users (or users who haven't yet spoken in Burt's presence)
$welcome_message = "Hi!"

#	Where the active counter data should be imported/exported to/from
$message_file_name = "message_counter.json"

def updateMessages(username)
	#
	#	Check to see if the 'user####' already has a message count 
	#
	#		- Create if no (return false to indicate new user)
	#
	#		- Increment if yes (return true to indicate incremented)
	
	if $message_counter.include?(username) then
		$message_counter[username]+=1
		return 1
	else 
		$message_counter[username]=1
		return 0
	end
end

def doPurge()
	#	Purge exports the active count to a backup
	#	before reinitializing $message_counter to {}
	doExport()
	$message_counter = {}
end

def checkPermissions(roles)
	#
	#	Users requesting access to !export and !purge must
	#	have the required permissions.
	#
	if (roles & $roles_allowed_to_make_changes).any?
		return 1
	else
		return 0
	end
end

def doExport()
	#	Export saves the contents of $message_counter to file
	#	and sets that file as the most recent copy
	export_filename = "#{$message_file_name.split(".")[0]}-#{Time.now.to_i.to_s}.json"
	puts("Exporting message counter to #{export_filename}")
	File.open(export_filename,"w") do |f|
		f.write($message_counter.to_json)
	end
	#	doImport looks for this file
	FileUtils.cp(export_filename, $message_file_name)
	puts("Reimporting file to refresh it")
	doImport()
end

def doImport()
	import_file = $message_file_name
	files = Dir.entries(".")
	message_data_files = files.select do |string|
		string.start_with?("message_counter")
	end.map do |element|
	  element
  end
	begin
		#
		#	This section is executed when no main json file is found
		#	it won't be required after the switch to database.
		#
		if not message_data_files.include?(import_file) then
			puts("No main counter file found")
			message_data_files.sort_by { |s| s.scan(/\d+/).first.to_i }
			puts "Using #{message_data_files[0]}"
			FileUtils.cp(message_data_files[0],import_file)
		end
		
		puts("Importing message counter data from file...")

		import_data = File.read(import_file)
		$message_handler = JSON.parse(import_data)
		puts $message_handler
	rescue
		puts("No data to import!")
		$message_handler = {}
	end
end



####
####	Main program entry
####

doImport()	#import main counter file

20.times {puts""} #clear any unrelated output off the screen

puts("\t\tBURT IS ONLINE\n\n")

####
####	Prepare event loop to communicate with discord API
####

bot.message do |event|
	message = event.message
	
	#	Burt doesn't need to keep a count of his own comments, skip those
	if message.author.id == bot.profile.id then
		next
	else
		# determine message author, attributes, and roles
		author = message.author
		
			#	Generate a unique id like beechundmoan5268
		username = author.username+author.discriminator		

			#Increment or start message count for author
		new_author = updateMessages(username)	# Inverted logic: new author is true @ 0

			#	Create a list of author roles 
			#	This might be wiser to include in the JSON
			#	to reduce processing overhead
			
		author_member_of = []
		author.roles.each do |role|
			author_member_of.push(role.name)
		end
		
		#	Send a welcome message to 'new' users
		if new_author == 0 then
			event.respond($welcome_message)
		end
		
		###
		###	Command Parser
		###
			#
			#	Looks for messages which start with a bang (!)
			#	
			#	Commands implemented so far:
			#	
			#	!count	asks for your own message count
			#
			#	!export	writes the active message count to file
			#
			#	!purge	exports the active count and resets the counter
			#
			#		
			#	Depending on desired functionality, this section may 
			#	need heavy revision.
			#	

		if message.content.start_with?("!") then
			data = message.content.split("!")[1]
			data_parse = data.split(" ")
			command = data_parse[0].downcase
			#	The following line will allow for arguments,
			#	such as 
			#	!count [username]
			#	
			#	Not yet implemented
			#
			#command_argument = data_parse[1]
			
			case command
				when "count"
					response = "#{username} has #{$message_counter[username]} messages"
					event.respond(response)
				when "export"
					if checkPermissions(author_member_of) == 1 then
						event.respond("Permission granted to export.\nExecuting.")
						doExport()
					else
						event.respond("**Permission DENIED**.\nYou have insufficient permissions to export the counter.")
					end
				when "purge"
					if checkPermissions(author_member_of) == 1 then
						event.respond("Permission granted to purge.\nExecuting.")
						doPurge()
					else
						event.respond("**Permission DENIED**.\nYou have insufficient permissions to purge the counter.")
					end
				when "debug"
					if checkPermissions(author_member_of) == 1 then
						event.respond("Permission granted to print debug data.\nExecuting.")
						bot.send_message("#debug", "#{$message_counter.to_s}")
					else
						event.respond("**Permission DENIED**.\nYou have insufficient permissions to purge the counter.")
					end

				else
					#
					#	Response to any bang-prefixed (!----) word that isn't a defined command
					#	
					event.respond("I didn't understand that command.")
			end
		else
			next
		end
	end
end

###
###	Enter event loop for API communication
###
bot.run
